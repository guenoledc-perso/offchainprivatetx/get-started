accounts="0x1778fef1adf67b8d904a73f64da4d29c1f13ec0b 0x63692126553a7100c4faa2b78d36b2757fe98d5e 0x663684a188d5f751f286c991f3704b478cec422e"
export SMARTCONTRACT_ADDRESS=""

echo "********** DOWNLOADING THE DOCKER IMAGES *****************************"
docker login -u gitlab+deploy-token-58776 -p LJCzVeousypLKEwbyni2 registry.gitlab.com
docker pull registry.gitlab.com/guenoledc-perso/offchainprivatetx/ethereum-privacy:latest
docker pull ethereum/client-go:alpine

# Start the public node alone
docker-compose down
echo "********** STARTING THE PUBLIC NODE *****************************"
docker-compose up -d publicnode
sleep 3

# Credit the accounts of the privacy nodes
echo "********** GIVING CREDIT TO PRIVACY NODES ACCOUNT ****************"
for account in $accounts
do
    echo "eth.sendTransaction({from:eth.coinbase, to:'$account', value:web3.toWei(10,'ether')})"
    docker-compose exec publicnode /geth attach --datadir /tmp --exec "eth.sendTransaction({from:eth.coinbase, to:'$account', value:web3.toWei(10,'ether')})"
done

# Deploy the smart contract
sleep 2
echo "********** DEPLOYING THE PUBLIC SMART CONTRACT *******************"
docker-compose run --rm privacynode1  --config /privacy/conf/default-conf.json --publicnode.account 0x1778fef1adf67b8d904a73f64da4d29c1f13ec0b --publicnode.deploy|grep "SMART CONTRACT CREATED AT"|grep -o -E "0x[0-9a-fA-F]{40}" > ./addressCreated.txt
export SMARTCONTRACT_ADDRESS=`cat ./addressCreated.txt`


echo "********** STARTING THE PRIVACY NODES ****************************"
docker-compose up -d 
sleep 5
echo "********** INITIALIZING THE PKI OF EACH NODE *********************"
services="privacynode1 privacynode2 privacynode3"
for service in $services
do
    docker-compose exec $service /bin/sh -c "/tmp/add-pubkey.sh http://privacynode1:8545 'node A'"
    docker-compose exec $service /bin/sh -c "/tmp/add-pubkey.sh http://privacynode2:8545 'node B'"
    docker-compose exec $service /bin/sh -c "/tmp/add-pubkey.sh http://privacynode3:8545 'node C'"
done


echo "********** EVERYTHING IS STARTED AND READY*************************"

