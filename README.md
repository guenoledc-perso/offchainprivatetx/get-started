# Ethereum Privacy - Getting Started

Repo for developers / techs who want to start playing with Ethereum Privacy solution

## Understand the architecture
Ethereum Privacy is a process that runs on a machine (or VM, docker ...) and connects to an Ethereum node (as public node) via json rpc and exposes an http json rpc web3 handler.

![Architecture](images/architecture.png "High level architecture")



## A- Pre-requisites

Before installing the program, you must collect the following elements

### 1. Have docker engine installed
We will use docker and docker-compose to setup a test environment. If you need to install docker, refer to the [documentation](https://docs.docker.com/v17.12/install/).

Check you have docker installed
```sh
    docker --version
    docker-compose --version
```
Should output something like
```
    Docker version 18.09.2, build 6247962
    docker-compose version 1.23.2, build 1110ad01
```

### 2. Access to a public ethereum network
This can be Ropsten or mainnet networks or it can be your local private ethereum blockchain or even a ganache test setup.

You must therefore have one wallet with ethers on it to be able to interact with the node and the url of the json rpc endpoint.

Here we will use a standard geth node in docker in local PoA in dev mode (refer to [geth documentation](https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options) )

Do not run this now! It will be done in the docker compose
```sh
docker run --rm -it -p 8547:8545 ethereum/client-go:alpine --dev --rpc --rpcaddr "0.0.0.0"
```

### 3. Ensure access to registry.gitlab.com
You will need to be able to collect the Ethereum Privacy latest image. A read only registry token is provided in the `docker login` command below. This is part of the shell script as well.
```sh
docker login -u gitlab+deploy-token-58776 -p LJCzVeousypLKEwbyni2 registry.gitlab.com
docker pull registry.gitlab.com/guenoledc-perso/offchainprivatetx/ethereum-privacy:latest
```


## B. Launch the environment

Run the shell script that does it all
```sh
./setup-env.sh
```
It does the following (see the script for how exactly it does it)
- start geth as public network
- get some ethers for each of the local accounts
- deploy in the public network the PrivateTxStorage smart contract (and get its address)
- start the 3 privacy nodes (Node A, Node B, Node C)
- inject the public keys of each node in the PKI of each nodes

If you run it again, you will loose all your data and the environment will be fully reconstructed.

Note: No error management in the script. If something goes wrong, do it step by step in the console.

## C. Play with the tests
Go to the client folder and build the client
```sh
cd client
npm install
```

**play with the client :** `node main --help`

**_Ethereum Privacy Node - test program_**

  Using a simple key value smart contract 

**Options:**
```
  --url string             the json rpc url of the privacy node to connect to                            
  --listen                 set the flag to keep the program listening for keys                           
  --smartcontract string   the address of the key/value smart contract to interact with. "auto"          
                           (default) to let the program find the last one. "create" to deploy a new      
                           instance                                                                      
  --key string             a key to use to set in the smart contract. If not set no key/value will be    
                           set                                                                           
  --value string           value for the key. Ignored if no key is specified. If missing "NULL" will be  
                           used                                                                          
  -t, --target string[]    Target nodes to submit this key/value or smart contract creation to. Default  
                           [node A, node B, node C]                                                      
  -h, --help               Display this usage guide.   
```

For instance use 2 terminals:
**First terminal** create the contract and listen to new keys
```sh
node main.js --smartcontract create --listen --url http://localhost:8546
---- output ----
{ url: 'http://localhost:8546',
  listen: true,
  smartcontract: 'create',
  value: 'NULL',
  target: [ 'node A', 'node B', 'node C' ] }
Connected to local privacy node Ethereum-Standalone-Node/v0.0.1/6000 at http://localhost:8546
Working with new wallet 0x389da882cfa681a0f4d60458fa7236fa1285fca2
hash= 0x6f049df28bfe03dbde7aca556ee951e623f15e496485fe38096819de16fae1ad
Smart contract created: 0xce21b69c10f32557c01b32298f085f1af4878e32
Starting listening. CTRL+C to stop.
KEY:hello VALUE:world NEW SET BY:0x059a49ec48972d5e1a6408295a7448fa8f1613f2 BLOCK:15  <<-- appears after the second terminal command
```

**Second terminal** find the contract and set a key value pair
```sh
node main.js --smartcontract auto --key hello --value world --url http://localhost:8547
---- output ----
{ url: 'http://localhost:8547',
  listen: false,
  smartcontract: 'auto',
  value: 'world',
  target: [ 'node A', 'node B', 'node C' ],
  key: 'hello' }
Connected to local privacy node Ethereum-Standalone-Node/v0.0.1/6000 at http://localhost:8547
Working with new wallet 0x059a49ec48972d5e1a6408295a7448fa8f1613f2
Smart contract address found at 0xce21b69c10f32557c01b32298f085f1af4878e32
hash= 0x915bd11a36eb658fce8bd59980c62fd49b76787eca46947ae9dbb734e6b50453
Value set status=1 
Closing the program
```

## Clean up your environment
Just execute the following to remove you docker running container
```sh
docker-compose down
```