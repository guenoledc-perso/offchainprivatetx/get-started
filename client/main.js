
const async = require('async')
const Web3 = require('web3')
const SM = require('./lib/SmartContract')

const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

let optionDefinitions = [
    { name: 'url', type: String, defaultValue:'http://localhost:8546', description: 'the json rpc url of the privacy node to connect to'},
    { name: 'listen', type: Boolean, defaultValue:false, description: 'set the flag to keep the program listening for keys'},
    { name: 'smartcontract', type: String, defaultValue:'auto', description: 'the address of the key/value smart contract to interact with. "auto" (default) to let the program find the last one. "create" to deploy a new instance'},
    { name: 'key', type: String,  description: 'a key to use to set in the smart contract. If not set no key/value will be set'},
    { name: 'value', type: String, defaultValue:'NULL', description: 'value for the key. Ignored if no key is specified. If missing "NULL" will be used'},
    { name: 'target', alias:'t', type: String, multiple:true, defaultValue:['node A', 'node B', 'node C'], description: 'Target nodes to submit this key/value or smart contract creation to. Default [node A, node B, node C]'},
    { name: 'proof', type: String, description: 'extract the transaction proof of a private tx hash and exit'},
    {
      name: 'help',
      alias: 'h',
      type: Boolean,
      description: 'Display this usage guide.'
    }
  ]
let options = commandLineArgs(optionDefinitions) 
if (options.help) {
    const usage = commandLineUsage([
      {
        header: 'Ethereum Privacy Node - test program',
        content: 'Using a simple key value smart contract'
      },
      {
        header: 'Options',
        optionList: optionDefinitions
      },
      {
        content: 'Project home: {underline https://gitlab.com/guenoledc-perso/offchainprivatetx}'
      }
    ])
    console.log(usage)
    process.exit(1)
  } else {
    console.log(options)
  }
  

let web3 = new Web3( new Web3.providers.HttpProvider(options.url) )
let eth = web3.eth
console.log('Connected to local privacy node',web3.version.node, 'at', options.url)
console.log('Working with new wallet', SM.newWallet.getAddressString())

let activities = []
let currentSmartContractAddress=null

if(options.smartcontract=='auto') activities.push(findSmartContract)
else if(options.smartcontract=='create') activities.push(createSmartContract)
else if(options.smartcontract.match(/^0x[0-9a-fA-F]{40}$/))
    currentSmartContractAddress = options.smartcontract
else {
    console.error('impossible to use the address provided:'+options.smartcontract)
    process.exit(1)
}

if(options.key) activities.push(setKeyValuePair)

if(options.listen) activities.push(listenToNewKeyValuePair)

if(options.proof) activities = [getTransactionProof]

let rpcId=1
function rpcCall(method, params, onCompletion) {
    web3.currentProvider.sendAsync({jsonrpc:"2.0", id:rpcId++, method:method, params:params}, (err, response)=>{
        if(err) onCompletion(err, null)
        else if(response.error) onCompletion(response.error, null)
        else onCompletion(null, response.result)
    })
}

function sendRawTransaction(signedtx, onReady) {
    rpcCall('eth_sendRawTransaction', ['0x'+signedtx.serialize().toString('hex'),{privateFor:options.target}], (err, hash)=>{
        console.log('hash=', hash)
        onReady(err, hash)
    })
}
function waitOneTxHash(hash, onFound) {
    if(!hash) return onFound('not a valid hash provided')
    let maxDuration=10000, durationStep=200
    let interval= setInterval( ()=>{
        maxDuration -= durationStep
        if(maxDuration<=0) {
            clearInterval(interval)
            return onFound('timeout')
        } else {
            eth.getTransactionReceipt(hash, (err, receipt)=>{
                if(err) {
                    clearInterval(interval)
                    return onFound(err)
                }
                if(receipt) {
                    clearInterval(interval)
                    return onFound(null, receipt)
                }
            })
        }
    }, durationStep)
    
}

function createSmartContract(cb) {
    let signedtx = SM.makeNewTx(null, SM.newWallet)
    if(!signedtx) return cb('cannot create the transaction')
    async.seq(
        cb=>cb(null, signedtx),
        sendRawTransaction,
        waitOneTxHash,
        (receipt, cb)=>{
            console.log('Smart contract created: '+receipt.contractAddress)
            currentSmartContractAddress=receipt.contractAddress
            cb(null)
        }
    )(err=>{
        if(err) console.error('Fail creating smart contract:',err)
        cb(null)
    })
    
}

function findSmartContract(cb) {
    function findOnBlock(block, onFound) {
        let address=null
        async.detectSeries(block.transactions, 
            (hash, cb)=>eth.getTransactionReceipt(hash, (err, receipt)=>{
                if(receipt && receipt.contractAddress) address=receipt.contractAddress
                cb(err, address)
            }),
            (err, hash)=>onFound(err, address) )
    }
    function findOnBlocks(number, onFound) {
        eth.getBlock(number, false, (err, block)=>{
            if(err) return onFound(err)
            if(block) findOnBlock(block, (err, address)=>{
                if(err) return onFound(err)
                if(!address) findOnBlocks(number-1, onFound)
                else onFound(null, address)
            })
        })
    }
    async.seq(
        cb=>eth.getBlockNumber(cb),
        (number, cb)=>findOnBlocks(number, cb),
        (address, cb)=>{
            if(address) console.log('Smart contract address found at', address)
            currentSmartContractAddress=address
            cb(null)
        }
    )(err=>{
        cb()
    })
}


function setKeyValuePair(cb) {
    let signedtx = SM.makeSetTx(currentSmartContractAddress, options.key, options.value, null, SM.newWallet)
    if(!signedtx) return cb('cannot create the transaction')
    async.seq(
        cb=>cb(null, signedtx),
        sendRawTransaction,
        waitOneTxHash,
        (receipt, cb)=>{
            console.log('Value set status='+receipt.status, receipt.exception)
            cb(null)
        }
    )(err=>{
        if(err) console.error('Fail setting the value:',err)
        cb(null)
    })
}
let listener
function listenToNewKeyValuePair(cb) {
    console.log('Starting listening. CTRL+C to stop.')
    listener = SM.listenEvent(eth, currentSmartContractAddress,
        log=>{
            console.log('KEY:'+log.args.key, 'VALUE:'+log.args.value, log.args.created?'NEW SET':'CHANGED','BY:'+log.args.owner, 'BLOCK:'+log.blockNumber)
        },
        err=>{
            console.log('Error at log event:', err)
            cb(err)
        })

    //setTimeout(cb, 2000)
}

function getTransactionProof(cb) {
    rpcCall('admin_getTransactionProof', [options.proof], (err, proof)=>{
        if(!err) console.log('proof of '+options.proof, JSON.stringify(proof, null, 2))
        cb(err)
    })
}


async.series( activities, (err)=>{
    console.log('Closing the program')
})

function gracefullStop() {
    // do something here
    listener.stopWatching(err=>{})
}
// INSTALL PROCESS HANDLING
process.on('uncaughtException', (err) => {
    console.log('Caught exception:', err);
});
process.on('SIGTERM', ()=>{
    console.log('Stopping process gracefully requested via SIGTERM')
    gracefullStop()
})
process.on('SIGINT', ()=>{
    console.log('Stopping process gracefully requested via SIGINT')    
    gracefullStop()
})
